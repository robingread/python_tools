import argparse
import matplotlib.pyplot as plt
import pandas as pd


def plot_hist_2d(x, num_bins=10, axis=None, x_label='x', y_label='y', title=None, label=None, *args, **kwargs):
    _show_plot = bool(axis is None)
    ax = axis

    if ax is None:
        fig = plt.figure()
        ax = fig.gca()

    ax.hist(x, bins=num_bins, label=label, *args, **kwargs)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.grid(True)
    if title is not None:
        ax.set_title(title)
    ax.legend()

    if _show_plot:
        plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=str, help="The CSV file to read/plot")
    parser.add_argument("x", type=str, help="Column title for x axis data")
    parser.add_argument('-bins', type=int, default="10", help="Number of bins")
    parser.add_argument('-xl', '--x_label', type=str, default=None, help='Optional x-axis label')
    parser.add_argument('-yl', '--y_label', type=str, default='frequency', help='Optional y-axis label')
    parser.add_argument('-t', '--title', type=str, default=None, help='Optional figure title')
    args = parser.parse_args()

    # Load the data
    data = pd.read_csv(args.file)

    # Set the title and axis labels
    x_label = args.x if args.x_label is None else args.x_label
    y_label = args.y_label
    title = args.file if args.title is None else args.title

    # Plot
    plot_hist_2d(x=data[args.x], num_bins=args.bins, x_label=x_label, y_label=args.y_label, title=title)
