import argparse
import matplotlib.pyplot as plt
import pandas as pd


def scatter_xy_2d(x, y, axis=None, x_label='x', y_label='y', title=None, label=None):
    """
    """
    _show_plot = bool(axis is None)
    ax = axis

    if ax is None:
        fig = plt.figure()
        ax = fig.gca()

    ax.scatter(x, y, label=label)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.grid(True)
    if title is not None:
        ax.set_title(title)
    ax.legend()

    if _show_plot:
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=str, help="The CSV file to read/plot")
    parser.add_argument("-x", type=str, default="x", help="Column title for x axis data")
    parser.add_argument("-y", type=str, default="y", help="Column title for y axis data")
    parser.add_argument('-xl', '--x_label', type=str, default=None, help='Optional x-axis label')
    parser.add_argument('-yl', '--y_label', type=str, default=None, help='Optional y-axis label')
    parser.add_argument('-t', '--title', type=str, default=None, help='Optional figure title')
    args = parser.parse_args()

    data = pd.read_csv(args.file)
    scatter_xy_2d(x=data[args.x],
                  y=data[args.y],
                  x_label=args.x if args.x_label is None else args.x_label,
                  y_label=args.y if args.y_label is None else args.y_label,
                  title=args.file if args.title is None else args.title)
