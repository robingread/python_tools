import argparse
import matplotlib.pyplot as plt
from toolshed.data import DataFile2D
import pandas as pd


def plot_xy_2d(x, y, axis=None, x_label='x', y_label='y', title=None, label=None, legend_location=1, *args, **kwargs):
    """
    """
    _show_plot = bool(axis is None)
    ax = axis

    if ax is None:
        fig = plt.figure()
        ax = fig.gca()

    ax.plot(x, y, label=label, *args, **kwargs)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.grid(True)
    if title is not None:
        ax.set_title(title)
    ax.legend(loc=legend_location)

    if _show_plot:
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=str, help="The CSV file to read/plot")
    parser.add_argument("-x", type=str, default="x", help="Column title for x axis data")
    parser.add_argument("-y", type=str, default="y", help="Column title for y axis data")
    parser.add_argument('-xl', '--x_label', type=str, default=None, help='Optional label for the x axis')
    parser.add_argument('-yl', '--y_label', type=str, default=None, help='Optional label for the y axis')
    parser.add_argument('-t', '--title', type=str, default=None, help='Optional plot title label')
    args = parser.parse_args()

    data = pd.read_csv(args.file)
    plot_xy_2d(x=data[args.x],
               y=data[args.y],
               x_label=args.x if args.x_label is None else args.x_label,
               y_label=args.y if args.y_label is None else args.y_label,
               title=args.file if args.title is None else args.title)
