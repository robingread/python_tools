import argparse
import matplotlib.pyplot as plt
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D


def plot_xyz_3d(x, y, z, axis=None, x_label='x', y_label='y', z_label='z', title="", label=None, *args, **kwargs):
    """
    """
    _show_plot = bool(axis is None)
    ax = axis

    if ax is None:
        fig = plt.figure()
        ax = fig.gca(projection='3d')

    ax.plot(x, y, z, label=label, *args, **kwargs)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_zlabel(z_label)
    ax.grid(True)
    ax.set_title(title)
    ax.legend()

    if _show_plot:
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=str, help="The CSV file to read/plot")
    parser.add_argument("-x", type=str, default="x", help="Column title for x axis data")
    parser.add_argument("-y", type=str, default="y", help="Column title for y axis data")
    parser.add_argument("-z", type=str, default="z", help="Column title for z axis data")
    parser.add_argument('-xl', '--x_label', type=str, default=None, help='Optional label for x axis')
    parser.add_argument('-yl', '--y_label', type=str, default=None, help='Optional label for y axis')
    parser.add_argument('-zl', '--z_label', type=str, default=None, help='Optional label for z axis')
    parser.add_argument('-t', '--title', type=str, default=None, help='Optional plot title')
    args = parser.parse_args()

    data = pd.read_csv(args.file)
    plot_xyz_3d(x=data[args.x],
                y=data[args.y],
                z=data[args.z],
                x_label=args.x if args.x_label is None else args.x_label,
                y_label=args.y if args.y_label is None else args.y_label,
                z_label=args.z if args.z_label is None else args.z_label,
                title=args.file if args.title is None else args.title)
