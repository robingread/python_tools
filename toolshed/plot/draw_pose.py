from toolshed.math import Vector3D, Rotation3D, ExtrinsicRotation3D, Transform3D
import numpy as np
import toolshed.util.log as log
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def draw_3dof_pose(axis, x, y, z):
    draw_6dof_pose(axis=axis, x=x, y=y, z=z, rx=0, ry=0, rz=0)


def draw_6dof_pose(axis, x, y, z, rx, ry, rz, scale=0.05):
    def range(vals):
        return vals[1] - vals[0]

    def do_transform(T, p):
        p = np.hstack((p.matrix, np.matrix([[1]])))
        p = t.matrix.dot(p.transpose())
        return Vector3D(x=p.item(0), y=p.item(1), z=p.item(2))

    p = Vector3D(x=x, y=y, z=z)
    t = Transform3D(rx=rx, ry=ry, rz=rz)

    x_scale = range(axis.get_xlim()) * scale
    x_ = Vector3D(x=3)
    px = do_transform(t, x_)
    px = Vector3D(x=p.x+px.x,
                  y=p.y+px.y,
                  z=p.z+px.z)

    y_scale = range(axis.get_ylim()) * scale
    y_ = Vector3D(y=3)
    py = do_transform(t, y_)
    py = Vector3D(x=p.x+py.x,
                  y=p.y+py.y,
                  z=p.z+py.z)

    z_scale = range(axis.get_zlim()) * scale
    z_ = Vector3D(z=3)
    pz = do_transform(t, z_)
    pz = Vector3D(x=p.x+pz.x,
                  y=p.y+pz.y,
                  z=p.z+pz.z)

    axis.plot([p.x, px.x],
            [p.y, px.y],
            [p.z, px.z], color="r")

    axis.plot([p.x, py.x],
            [p.y, py.y],
            [p.z, py.z], color="g")

    axis.plot([p.x, pz.x],
            [p.y, pz.y],
            [p.z, pz.z], color="b")


if __name__ == '__main__':
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    ax.set_xlim([-1, 10])
    ax.set_ylim([-1, 10])
    ax.set_zlim([-1, 10])

    draw_6dof_pose(axis=ax, x=0, y=0, z=0, rx=0, ry=0, rz=0)
    draw_6dof_pose(axis=ax, x=10, y=0, z=0, rx=0.78, ry=0.78, rz=0.78)

    plt.show()