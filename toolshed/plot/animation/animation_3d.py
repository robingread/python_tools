from animated_plot import AnimatedPlot
from mpl_toolkits.mplot3d import Axes3D


class AnimatedPlot3d(AnimatedPlot):
    def __init__(self, *args, **kwargs):
        super(AnimatedPlot3d, self).__init__(*args, **kwargs)

    def add_plot(self, name, *args, **kwargs):
        self._lines_by_name[name] = self.axis.plot([], [], [], *args, **kwargs)[0]

    def _get_axis(self):
        return self.fig.gca(projection='3d')
