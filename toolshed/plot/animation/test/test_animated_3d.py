from toolshed.plot.animation import AnimatedPlot3d
import matplotlib.pyplot as plt
import numpy as np


class TestAnimatedPlot3d(AnimatedPlot3d):
    def __init__(self, *args, **kwargs):
        super(TestAnimatedPlot3d, self).__init__(*args, **kwargs)

    def on_start(self):
        pass

    def on_animate(self, i):
        x = np.arange(0, 20, 0.05)
        y = np.sin(x+(i*0.1))
        z = np.cos(x+(i*0.1))**2
        self.axis.set_xlim([np.min(x)-0.1, np.max(x)+0.1])
        self.axis.set_ylim([np.min(y)-0.1, np.max(y)+0.1])
        self.axis.set_zlim([np.min(z)-0.1, np.max(z)+0.1])
        self.lines['test_line'].set_data(x, y)
        self.lines['test_line'].set_3d_properties(z)


if __name__ == '__main__':
    fig = plt.figure()
    a = TestAnimatedPlot3d(figure=fig, interval_ms=50)
    a.add_plot('test_line', label='test line', marker='o')
    a.axis.legend()
    a.axis.grid(True)
    a.start_animation()
    plt.show()