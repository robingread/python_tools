import matplotlib.animation as animation
import matplotlib.pyplot as plt
from toolshed.util.log import log


class AnimatedPlot(object):
    """
    Base class for animated line plots. Bare bones functionality is implemented here. This class should be inherited
    by other classes that implement the specifics for an animated plot.
    """
    def __init__(self, figure=None, interval_ms=200, frame_count=None):
        self._fig = figure if figure is not None else self._get_figure()
        self._axis = self._get_axis()
        self._animation = None
        self._interval_ms = interval_ms
        self._frame_count = frame_count
        self._lines_by_name = dict()
        self._points_by_name = dict()

    ##############
    # Properties #
    ##############
    @property
    def axis(self):
        """ """
        return self._axis

    @property
    def fig(self):
        """ """
        return self._fig

    @property
    def interval_ms(self):
        """ """
        return self._interval_ms

    @property
    def frame_count(self):
        """ """
        return self._frame_count

    @property
    def line_names(self):
        """ """
        return self._lines_by_name.keys()

    @property
    def lines(self):
        """ """
        return self._lines_by_name

    @property
    def point_names(self):
        """ """
        return self._points_by_name.keys()
    
    @property
    def points(self):
        return  self._points_by_name

    ##################
    # Public methods #
    ##################
    def add_plot(self, name, *args, **kwargs):
        """
        """
        self._lines_by_name[name] = self.axis.plot([], [], *args, **kwargs)[0]

    def add_scatter(self, name, *args, **kwargs):
        self._points_by_name[name] = self.axis.scatter([], [], *args, **kwargs)

    def start_animation(self):
        """
        """
        self.on_start()
        self._animation = animation.FuncAnimation(fig=self.fig, func=self._animate,
                                                  frames=self.frame_count, interval=self.interval_ms)

    def on_start(self):
        """
        Override this method.
        """
        pass

    def on_animate(self, i):
        """
        Override this method.
        """
        pass

    ###################
    # Private methods #
    ###################
    def _animate(self, i):
        log.debug_value('Animating, i', i)
        self.on_animate(i)
        return self.lines.values()

    def _get_axis(self):
        """
        """
        return self.fig.gca()

    def _get_figure(self):
        """ """
        return plt.figure()
