import numpy as np


class Matrix(object):
    def __init__(self, *args, **kwargs):
        self._np_matrix = self.get_numpy_matrix()

    def __mul__(self, other):
        raise NotImplementedError()

    @property
    def matrix(self):
        return self._np_matrix

    @property
    def inverse_matrix(self):
        return self._np_inverse_matrix

    def get_numpy_matrix(self):
        return np.matrix([[0]])


