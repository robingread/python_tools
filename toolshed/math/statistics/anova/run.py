import pandas as pd
import statsmodels.api as sm
from statsmodels.formula.api import ols
from statsmodels.stats.multicomp import pairwise_tukeyhsd
from methods import *


def _run_anova(data, dvar, ivars):
    """
    Run the ANOVA itself and print the result to screen.

    Args:
        data (pandas.DataFrame): The raw data.
        dvar (str): Dependent variable name.
        ivars (list(str)): List of independent variable names.
    """
    # Create the formula
    formula = create_ols_formula(ivars=ivars, dvar=dvar)

    # Run the ANOVA
    data_lm = ols(formula, data=data).fit()
    table = sm.stats.anova_lm(data_lm, typ=2)

    log.info_value('ANOVA OLS formaula', formula)
    log.info('ANOVA Results\n{0}'.format(table))


def _run_multicomparison_tests(data, dvar, ivars):
    """
    """
    # TODO - Doc strings
    # TODO - print out the group numbers and conditions
    # TODO - print out the group means and CI values
    data, group_combos = group_unique_ivar_combos(data, ivars)

    # Perform the pairwise test
    res2 = pairwise_tukeyhsd(data[dvar], data['group_combos'])
    log.info('\nMulti-Comparison Results\n{0}'.format(res2))

    # Print the group combo data
    group_lines = get_group_combos_print_lines(group_combos)
    log.info(group_lines)


def run(file_path=None, data_frame=None, dvar=None, ivars=None, pre_process_func=None, plot=True):
    """
    Args:
        file_path (str):
        data_frame (pandas.DataFrame):
        dvar (str):
        ivars (str):
        pre_process_func (callable): Optional, a function that pre-processes the data. Useful if you want to add/edit data before running the ANOVA.
        plot (bool): Optional, whether to display a plot of the means.
    """
    # Load the data file
    if data_frame is None and isinstance(file_path, str):
        data = pd.read_csv(file_path)
    elif isinstance(data_frame, pd.DataFrame) and file_path is None:
        data = data_frame
    else:
        raise NotImplementedError()

    # Run the pre-process function if it is callable.
    data = data if not callable(pre_process_func) else pre_process_func(data)

    # Run the ANVOA
    _run_anova(data=data, ivars=ivars, dvar=dvar)
    for i in range(1, len(ivars)+1):
        interaction_means(inter_N=i, data=data, dvar=dvar, ivars=ivars)

    # Get all the group names, and add these to the data
    try:
        _run_multicomparison_tests(data=data, dvar=dvar, ivars=ivars)
    except Exception as e:
        log.warning('Got exception running Multicomparison: {0}, {1}'.format(e.__class__, e.message))

    # Show all the plots
    if plot:
        plt.show()
