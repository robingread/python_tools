from toolshed.util.log import log
from collections import OrderedDict
import numpy as np
import itertools
import matplotlib.pyplot as plt
from ..bootstrap import bootstrap


def create_nominal_ivars(ivars):
    """
    Create the independent variables for the OLS formula.

    Args:
        ivars (list(str)): String list of independent variables.

    Returns:
        str: Formula string.
    """
    v = ['C({0})'.format(d) for d in ivars]
    ret = ''
    for i in range(len(v)):
        ret += v[i] if i == (len(v) - 1) else v[i] + ' * '
    return ret


def create_ols_formula(dvar, ivars):
    """
     Create the OLS formula into the format 'ivar ~ dvar_1 * dvar_2 ... dvar_n'

     Args:
         dvar (str): The dependent variable - the thing that was measured.
         ivars (list(str)): The independent variable(s) - the things that were controlled/varied.

    Returns:
        (str): OLS formula string.
    """
    assert isinstance(ivars, list), 'ivar must be a string'
    assert isinstance(dvar, str), 'dvars must be a list'
    return '{0} ~ {1}'.format(dvar, create_nominal_ivars(ivars=ivars))


def get_indices(data, ivar_dict, combo, index):
    """
    """
    # TODO - Doc strings
    i = index
    header = ivar_dict.keys()[i]
    value = combo[i]
    log.debug('{0}={1}'.format(header, value))
    ret = data[header][data[header] == value].index
    return ret


def get_group_combos_print_lines(group_combos):
    """
    """
    # TODO - Doc strings
    lines = list()
    for group, value in group_combos.items():
        line = 'group {0}: '.format(group)
        for k, v in value.items():
            line += '{0}={1}, '.format(k, v)
        lines += [line]
    return lines


def group_unique_ivar_combos(data, ivars):
    """
    """
    # TODO - Doc strings
    ivar_dict = OrderedDict()
    for d in ivars:
        ivar_dict[d] = data[d].unique().tolist()

    group_names = np.array([None for i in range(len(data))])
    group = 0
    group_dict = OrderedDict()
    for combo in list(itertools.product(*ivar_dict.values())):
        indices = None
        group_dict[group] = {}
        for i in range(len(combo)):
            idx = get_indices(data, ivar_dict, combo, i)
            indices = idx if indices is None else idx.intersection(indices)
            group_dict[group][ivar_dict.keys()[i]] = combo[i]
        group_names[indices] = group
        group += 1

    assert None not in group_names.tolist()
    data['group_combos'] = group_names

    return data, group_dict


def interaction_means(inter_N, data, dvar, ivars, plot=True):
    """
    """
    # TODO - Doc strings
    # Create a dictionary containing all the unique values for each independent variable.
    ivar_dict = OrderedDict()
    for i in ivars:
        ivar_dict[i] = data[i].unique().tolist()

    # Get the combinations of the independent variables
    inter_combos = list(itertools.combinations(ivars, inter_N))

    # Create the plot varibles
    plot_count = len(inter_combos)  # Number of plots, one for each level of interaction.
    plot_counter = 0
    fig = plt.figure()

    for combo in inter_combos:
        combo_dict = OrderedDict()
        for c in combo:
            combo_dict[c] = ivar_dict[c]

        ax = fig.add_subplot(plot_count, 1, plot_counter)

        mean_cond = []
        mean_vals = []
        mean_ci = []

        log.info_value('Interactions', combo)
        for key_combo in list(itertools.product(*combo_dict.values())):
            idx = None

            for i in range(len(key_combo)):
                factor = combo_dict.keys()[i]
                factor_value = key_combo[i]
                _idx = data[factor][data[factor] == factor_value].index
                idx = _idx if idx is None else _idx.intersection(idx)

            # Bootstrap the data in these indices
            _data = np.array(data[dvar].loc[idx])
            # TODO - Note the nanmean
            boot = bootstrap(_data, 10000, np.median)
            log.info('\t {0} bootstrap results: mean={1:.3f}, std={2:.3f}, 95% ci=[{3:.3f}, {4:.3f}]'.format(str(key_combo).upper(),
                                                                                                             np.mean(boot.values),
                                                                                                             np.std(boot.values),
                                                                                                             boot.min_ci,
                                                                                                             boot.max_ci))
            # Get the
            mean_cond += [str(key_combo)]
            mean_vals.append(np.nanmean(boot.values))
            mean_ci += [(boot.max_ci - boot.min_ci) / 2.0]

        ind = np.arange(len(list(itertools.product(*combo_dict.values()))))  # the x locations for the groups
        width = 0.75       # the width of the bars
        ax.bar(ind, mean_vals, width, yerr=mean_ci, color='y')
        # ax.bar(ind, mean_vals, width, color='y')
        ax.set_xticks(ind+(width/2.0))
        ax.set_xticklabels(mean_cond)
        ax.set_ylabel(dvar)
        ax.set_xlabel(combo)
        ax.grid(True)
        plot_counter += 1
