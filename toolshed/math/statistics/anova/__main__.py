import argparse
from run import run

parser = argparse.ArgumentParser()
parser.add_argument('file', type=str, help='The CSV file to open and load')
parser.add_argument('-i', '--ivars', metavar='N', type=str, nargs='+', help='The independent variable(s) - the thing(s) controlled/varied')
parser.add_argument('-d', '--dvar', type=str, help='The dependent variable - the thing that was measured.')
args = parser.parse_args()

run(file_path=args.file, dvar=args.dvar, ivars=args.ivars)
