import argparse
from run import run


parser = argparse.ArgumentParser()
parser.add_argument('file', type=str, help='The file to open and load')
parser.add_argument('-x', '--x_axis', type=str, help='The varibale to plot on the x_axis')
parser.add_argument('-y', '--y_axis', type=str, help='The independent variable to plot on the y_axis')
parser.add_argument('-f', '--factors', metavar='N', type=str, nargs='+', help='The dependent variables')
parser.add_argument('-w', '--window_width', type=float, help='Window width for values along x-axis')
parser.add_argument('-pp', '--plot_perfect', help='Plot the line where x=y', action='store_true')
args = parser.parse_args()


run(file_path=args.file,
    x_axis=args.x_axis,
    y_axis=args.y_axis,
    factors=args.factors,
    window_width=args.window_width,
    plot_x_equal_y=args.plot_perfect)
