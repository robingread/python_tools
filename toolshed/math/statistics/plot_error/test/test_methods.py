import unittest
from collections import OrderedDict
import pandas as pd
from toolshed.math.statistics.plot_error.methods import *


class TestMethods(unittest.TestCase):
    def test_get_data_indices(self):
        data = pd.read_csv('test.csv')
        ret = get_data_indices(data, dvar='speed', dvar_value='fast').tolist()
        ans = [0, 1, 2, 3, 4, 5, 6, 7]
        self.assertEqual(ans, ret)

    def test_get_data_subset(self):
        data = pd.read_csv('test.csv')

        dvar_dict = {'camera': 'up', 'speed': 'fast'}
        # dvar_pairs.append(('camera', 'up'))
        # dvar_pairs.append(('speed', 'fast'))

        ret = get_data_subset(data=data, dvar_values=dvar_dict)

        exp_data = {'time': [0, 1, 2, 3],
                    'camera': ['up' for i in range(4)],
                    'speed': ['fast' for i in range(4)],
                    'distance': [10.1, 10.2, 10.3, 10.4]}
        ans = pd.DataFrame(exp_data)

        self.assertEqual(len(ans), len(ret))
        for k in ['speed', 'time', 'camera', 'distance']:
            self.assertEqual(ans[k].tolist(), ret[k].tolist())

    def test_get_binned_data(self):
        data = pd.read_csv('test.csv')

        ans = get_binned_data(data=data, window_width=1, bin_axis='time', ivar='distance')

        ret = OrderedDict()
        ret[0.5] = [10.1, 15.1, 20.1, 25.1]
        ret[1.5] = [10.2, 15.2, 20.2, 25.2]
        ret[2.5] = [10.3, 15.3, 20.3, 25.3]
        ret[3.5] = [10.4, 15.4, 20.4, 25.4]

        self.assertEqual(ret, ans)

    def test_get_unique_dvar_values(self):
        data = pd.read_csv('test.csv')

        ans = get_unique_dvar_values(data, ['speed', 'camera'])
        ret = {'speed': ['fast', 'slow'], 'camera': ['up', 'forward']}

        self.assertEqual(ans, ret)

    def test_get_dvar_combos(self):
        data = pd.read_csv('test.csv')

        ans = get_dvar_combos(get_unique_dvar_values(data, ['speed', 'camera']))

        ret = list()
        ret.append({'camera': 'up', 'speed': 'fast'})
        ret.append({'camera': 'up', 'speed': 'slow'})
        ret.append({'camera': 'forward', 'speed': 'fast'})
        ret.append({'camera': 'forward', 'speed': 'slow'})

        self.assertEqual(ans, ret)
