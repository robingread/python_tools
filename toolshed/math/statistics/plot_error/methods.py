import numpy as np
import itertools
from collections import OrderedDict


def get_data_indices(data, dvar, dvar_value):
    """
    Get the indices of data with specific column values.

    Args:
        data (pandas.DataFrame): Data to search.
        dvar (str): Column name.
        dar_value (str): Values column should have.

    Returns:
        pandas.Index: Indices in data.
    """
    return data[dvar][data[dvar] == dvar_value].index


def get_data_subset(data, dvar_values):
    """
    Get a data subset.

    Args:
        data (pandas.DataFrame): Data to subsection.
        dvar_values (dict): Dictionary of python pairs with the format {column_name: column_value).

    Returns:
        pandas.DataFrame: DataFrame containing the subset data.
    """
    print('\nlen(data) = {0}'.format(len(data)))
    idx = None
    for var, val in dvar_values.items():
        print('{0}: {1}'.format(var, val))
        i = get_data_indices(data, dvar=var, dvar_value=val)
        print('last index: {0}'.format(i[-1]))
        idx = i if idx is None else i.intersection(idx)
    return data.loc[idx] if idx is not None else data


def get_binned_data(data, bin_axis, ivar, window_width=1):
    """
    Bin data along an axis.

    Args:
        data (pandas.DataFrame):
        ivar (str):
        dvar (str):
        window_width (float):

    Returns:
        dict():
    """
    min_ivar = np.min(data[bin_axis])
    max_ivar = np.max(data[bin_axis])

    ret = OrderedDict()
    for i in np.arange(min_ivar, (max_ivar + window_width), window_width):
        _min = i
        _max = _min + window_width
        _center = _min + (window_width / 2.0)

        _idx_min = data[bin_axis][data[bin_axis] >= _min].index
        _idx_max = data[bin_axis][data[bin_axis] < _max].index
        _idx = _idx_min.intersection(_idx_max)

        if len(_idx) > 0:
            ret[_center] = data[ivar][_idx].tolist()
    return ret


def get_unique_dvar_values(data, dvars):
    ret = dict()
    for d in dvars:
        ret[d] = data[d].unique().tolist()
    return ret


def get_dvar_combos(dvar_unique_vals):
    keys = dvar_unique_vals.keys()
    combos = list(itertools.product(*dvar_unique_vals.values()))

    ret = list()
    for c in combos:
        line = dict()
        for i in range(len(c)):
            line[keys[i]] = c[i]
        ret.append(line)
    return ret