import pandas as pd
import numpy as np
from toolshed.math.statistics.bootstrap import bootstrap as boots
from toolshed.util.log import log
from methods import *
import matplotlib.pyplot as plt


def run(file_path=None, data_frame=None,
        x_axis=None, y_axis=None,
        factors=None, window_width=1,
        pre_process_func=None,
        plot_x_equal_y=False,
        title=None,
        x_label=None,
        y_label=None):
    """
     Args:
         file_path (str): Optional
         data_frame (pandas.DataFrame): Optional
         x_axis (str): Optional
         y_axis (str): Optional
         factors (list): Optional
         window_width (int): Optional
    """

    # Some argument assertions
    assert isinstance(x_axis, str), 'x_axis argument must be a string'
    assert isinstance(y_axis, str), 'y_axis argument must be a string'

    # Load the data file
    if data_frame is None and isinstance(file_path, str):
        data = pd.read_csv(file_path)
    elif isinstance(data_frame, pd.DataFrame) and file_path is None:
        data = data_frame
    else:
        raise NotImplementedError()

    # Pre-process the data
    data = data if not callable(pre_process_func) else pre_process_func(data)

    # Log data about
    log.info_value('Data length', len(data))
    log.info_value('Factors', factors)
    log.info_value('X axis', x_axis)
    log.info_value('Y axis', y_axis)
    log.info_value('Window width', window_width)
    log.info_value('Title', title)
    log.info_value('Plot x=y', plot_x_equal_y)

    fig = plt.figure()
    ax = fig.gca()

    if factors is not None:
        unique_dvars = get_unique_dvar_values(data=data, dvars=factors)
        dvar_combos = get_dvar_combos(unique_dvars)
        for dc in dvar_combos:
            dat_subset = get_data_subset(data, dc)
            binned_data = get_binned_data(dat_subset, x_axis, y_axis, window_width=window_width)

            _x = binned_data.keys()
            b = [boots(np.array(d), statistic=np.mean) for d in binned_data.values()]
            _y = [np.mean(_b.values) for _b in b]
            _ci = [(_b.max_ci - _b.min_ci)/2.0 for _b in b]
            ax.errorbar(x=_x, y=_y, yerr=_ci, label=str(dc))

    else:
        binned_data = get_binned_data(data, x_axis, y_axis, window_width=window_width)
        _x = binned_data.keys()
        b = [boots(np.array(d), statistic=np.mean) for d in binned_data.values()]
        _y = [np.mean(_b.values) for _b in b]
        _ci = [(_b.max_ci - _b.min_ci)/2.0 for _b in b]
        ax.errorbar(x=_x, y=_y, yerr=_ci)
        ax.set_xlabel(x_axis)
        ax.set_ylabel(y_axis)
        ax.grid(True)
        ax.legend()

    #####################
    # Plot the x=y line #
    #####################
    if plot_x_equal_y:
        _min_x = np.min(data[x_axis])
        _max_x = np.max(data[x_axis])
        _x_vals = np.arange(_min_x, _max_x, window_width)
        ax.plot(_x_vals, color='red', label='x=y')

    if title is not None:
        ax.set_title(title)

    _x_label = x_axis if x_label is None else x_label
    ax.set_xlabel(_x_label)

    _y_label = y_axis if y_label is None else y_label
    ax.set_ylabel(_y_label)

    ax.grid(True)
    ax.legend()

    plt.show()

