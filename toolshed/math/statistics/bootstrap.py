import numpy as np
import numpy.random as npr


class BootstrapResult(object):
    def __init__(self, min_ci, max_ci, values, alpha):
        self._min_ci = min_ci
        self._max_ci = max_ci
        self._alpha = alpha
        self._values = values

    @property
    def alpha(self):
        return self._alpha

    @property
    def values(self):
        return self._values

    @property
    def min_ci(self):
        return self._min_ci

    @property
    def max_ci(self):
        return self._max_ci


def bootstrap(data, boot_num=10000, statistic=None, num_samples=None, alpha=0.05):
    n = len(data) if num_samples is None else num_samples

    idx = npr.randint(low=0, high=n, size=(boot_num, n))
    samples = data[idx]

    if statistic is None:
        return samples

    stat = np.sort(statistic(samples, 1))
    min_ci = stat[int((alpha/2.0)*boot_num)]
    max_ci = stat[int((1-alpha/2.0)*boot_num)]
    means = statistic(samples, 1)
    return BootstrapResult(min_ci=min_ci, max_ci=max_ci, values=means, alpha=alpha)


def plot_bootstrap(result):
    pass


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from toolshed.plot.plot_hist_2d import plot_hist_2d

    def get_dist_error():
        return np.array([120.59, 132.12, 123.5, 115.42, 118.65, 110, 102, 105, 119])

    x = get_dist_error()
    print('mean: {0}, std: {1}'.format(np.mean(x), np.std(x)))
    result = bootstrap(x, 1000000, np.mean, alpha=0.05)

    fig = plt.figure()
    ax = fig.gca()
    plot_hist_2d(x, 20, axis=ax)

    fig = plt.figure()
    ax = fig.gca()
    plot_hist_2d(result.values, 50, axis=ax, histtype='stepfilled')

    fig = plt.figure()
    ax = fig.gca()
    ax.plot([-0.03,0.03], [np.mean(x), np.mean(x)], 'r', linewidth=2)
    ax.scatter(0.1*(npr.random(len(x))-0.5), x)
    ax.plot([0.19,0.21], [result.min_ci, result.min_ci], 'r', linewidth=2)
    ax.plot([0.19,0.21], [result.max_ci, result.max_ci], 'r', linewidth=2)
    ax.plot([0.2,0.2], [result.min_ci, result.max_ci], 'r', linewidth=2)
    ax.set_xlim([-0.2, 0.3])
    ax.set_title('Bootstrap 95% CI for mean')
    plt.show()
