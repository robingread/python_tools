import numpy as np


class RollingAverage(object):
    """
    Rolling Average calculator, with the option of setting the size of the window, and the statistic that is calculated (e.g. mean, median, mode, ect).
    """
    def __init__(self, size, statistic=np.mean):
        assert callable(statistic), 'The statistic must be a callable object that returns a float.'

        self._size = size
        self._statistic = statistic
        self._buffer = list()

    ##############
    # Properties #
    ##############
    @property
    def value(self):
        """
        float or None: None if the buffer is empty, else the mean value
        """
        return None if self.length == 0 else self._statistic(self._buffer)

    @property
    def size(self):
        """
        int: Size of the buffer.
        """
        return self._size

    @property
    def _buffer_full(self):
        return self.length > self._size

    @property
    def buffer(self):
        """
        list(float): Buffer containing the last N elements added to the buffer.
        """
        return self._buffer

    @property
    def length(self):
        """
        int: Current length of the internal buffer
        """
        return len(self._buffer)

    ##################
    # Public Methods #
    ##################
    def append(self, value):
        """
        Append a value to the rolling average.

        Args:
            value (float): Value to append.
        """
        self._buffer.append(value)
        self._buffer = self._buffer if not self._buffer_full else self._buffer[1:]

    def set_size(self, size):
        """
        Set the size of the internal buffer.

        Args:
            size (int): Buffer size.
        """
        self._size = size
