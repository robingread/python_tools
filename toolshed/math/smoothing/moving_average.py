import numpy as np
import matplotlib.pyplot as plt


def get_gaussian_weights(x, amp=1, mean=0, sigma=1):
    """
    Get the weights of a Gaussian kernel.

    Args:
        x: Values to attribute a weighting to.
        amp: Amplitude scalar
        mean: Mean of the kernel
        sigma: Width of the kernel

    Returns:
        NumPy array containing the weights for each value of X
    """
    return amp*np.exp(-(x-mean)**2/(2*sigma**2))


def weighted_moving_average_bins(x, y, step_size=0.05, width=1):
    """
    Args:
        x: Data for the x axis (e.g. time)
        y: Data for the y axis (e.g. values of something, through time)
        step_size:
        width: Desired width of the Gaussian kernel
    """
    bin_centers = np.arange(np.min(x), np.max(x)-0.5*step_size, step_size) + 0.5*step_size
    bin_avg = np.zeros(len(bin_centers))

    for index in range(0, len(bin_centers)):
        bin_center = bin_centers[index]
        weights = get_gaussian_weights(x,mean=bin_center,sigma=width)
        bin_avg[index] = np.average(y, weights=weights)

    return bin_centers, bin_avg


def weighted_moving_average(x, y, width=1.0):
    ret = np.zeros(x.shape)
    for i in range(0, len(x)):
        weights = get_gaussian_weights(x=x, mean=x[i], sigma=width)
        ret[i] = np.average(y, weights=weights)
    return ret


if __name__ == '__main__':
    # first generate some datapoint for a randomly sampled noisy sinewave
    x = np.random.random(1000)*10
    noise = np.random.normal(scale=0.3, size=len(x))
    y = np.sin(x) + noise

    # plot the data
    plt.plot(x,y,'ro',alpha=0.3,ms=4,label='data')
    plt.xlabel('Time')
    plt.ylabel('Intensity')

    bins, average = weighted_moving_average_bins(x=x, y=y, step_size=0.5, width=0.05)
    plt.plot(bins, average,label='moving average')

    plt.show()
