import unittest
from toolshed.math.smoothing.rolling_average import RollingAverage


class TestRollingAverage(unittest.TestCase):
    def setUp(self):
        self.roller_size = 10
        self.roller = RollingAverage(self.roller_size)

    def test_init(self):
        self.assertEqual(self.roller.size, self.roller_size)
        self.assertFalse(self.roller._buffer_full)
        self.assertEqual(self.roller.length, 0)
        self.assertIsNone(self.roller.value)

    def test_append(self):
        for i in range(3):
            self.roller.append(i)
            self.assertFalse(self.roller._buffer_full)
            self.assertEqual(self.roller.length, i+1)
            self.assertIsNotNone(self.roller.value)

    def test_append_unit_full(self):
        for i in range(self.roller_size+2):
            self.roller.append(i)
        self.assertEqual(self.roller.length, self.roller_size)
        self.assertFalse(self.roller._buffer_full)


if __name__ == '__main__':
    unittest.main()
