import numpy as np
from ..matrix import Matrix


class Vector2d(Matrix):
    def __init__(self, x=0.0, y=0.0, *args, **kwargs):
        self._x = x
        self._y = y
        super(Vector2d, self).__init__(*args, **kwargs)

    def __str__(self):
        return "Vector2D({p.x}, {p.y})".format(p=self)

    def __add__(self, other):
        if isinstance(other, Vector2d):
            return Vector2d(x=self.x + other.x, y=self.y + other.y)
        elif isinstance(other, (int, float)):
            return Vector2d(x=self.x + other, y=self.y + other)

    def __sub__(self, other):
        if isinstance(other, Vector2d):
            return Vector2d(x=self.x - other.x, y=self.y - other.y)
        elif isinstance(other, (int, float)):
            return Vector2d(x=self.x - other, y=self.y - other)

    ##############
    # Properties #
    ##############
    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @property
    def components(self):
        return self.x, self.y

    @property
    def length(self):
        return np.sqrt(self.x**2 + self.y**2)

    ##################
    # Public methods #
    ##################
    def get_numpy_matrix(self):
        return np.matrix([self.x, self.y])

    def set(self, x, y):
        self.x = x
        self.y = y
