import unittest
from toolshed.math.math2d.vector2d import Vector2d


class TextVector2dAddition(unittest.TestCase):
    def test_vector_add_vector(self):
        v1, v2 = Vector2d(x=1, y=2), Vector2d(x=1, y=2)
        r = v1 + v2
        a = Vector2d(x=2, y=4)
        self.assertEqual(r.components, a.components)

    def test_vector_add_int(self):
        v1 = Vector2d(x=1, y=2)
        r = v1 + 10
        a = Vector2d(x=11, y=12)
        self.assertEqual(r.components, a.components)

    def test_vector_iadd_vector(self):
        v1, v2 = Vector2d(x=1, y=2), Vector2d(x=1, y=2)
        v1 += v2
        a = Vector2d(x=2, y=4)
        self.assertEqual(v1.components, a.components)

    def test_vector_iadd_int(self):
        v1 = Vector2d(x=1, y=2)
        v1 += 10
        a = Vector2d(x=11, y=12)
        self.assertEqual(v1.components, a.components)


class TextVector2dSubtraction(unittest.TestCase):
    def test_vector_sub_vector(self):
        v1, v2 = Vector2d(x=1, y=2), Vector2d(x=1, y=2)
        r = v1 - v2
        a = Vector2d(x=0, y=0)
        self.assertEqual(r.components, a.components)

    def test_vector_sub_int(self):
        v1 = Vector2d(x=1, y=2)
        r = v1 - 10
        a = Vector2d(x=-9, y=-8)
        self.assertEqual(r.components, a.components)

    def test_vector_iadd_vector(self):
        v1, v2 = Vector2d(x=1, y=2), Vector2d(x=1, y=2)
        v1 -= v2
        a = Vector2d(x=0, y=0)
        self.assertEqual(v1.components, a.components)

    def test_vector_iadd_int(self):
        v1 = Vector2d(x=1, y=2)
        v1 -= 10
        a = Vector2d(x=-9, y=-8)
        self.assertEqual(v1.components, a.components)
