import numpy as np
from ..matrix import Matrix


class Rotation2d(Matrix):
    """
    """
    def __init__(self, theta, *args, **kwargs):
        self._theta = theta
        super(Rotation2d, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "Rotation2d(theta={p.theta} rad)".format(p=self)

    ##############
    # Properties #
    ##############
    @property
    def theta(self):
        return self._theta

    ##################
    # Public methods #
    ##################
    def get_numpy_matrix(self):
        return get_rotation_matrix(self.theta)


def get_rotation_matrix(theta):
    """
    Args:
        theta:
    """
    return np.matrix([[np.cos(theta), -np.sin(theta)],
                      [np.sin(theta), np.cos(theta)]])
