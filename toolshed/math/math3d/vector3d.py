import numpy as np
from ..matrix import Matrix


class Vector3D(Matrix):
    """
    """
    def __init__(self, x=0.0, y=0.0, z=0.0, *args, **kwargs):
        self._x = x
        self._y = y
        self._z = z
        super(Vector3D, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "Vector3D(x={p.x}, y={p.y}, z={p.z})".format(p=self)

    ##############
    # Properties #
    ##############
    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @property
    def z(self):
        return self._z

    @z.setter
    def z(self, value):
        self._z = value

    @property
    def components(self):
        return self.x, self.y, self.z

    ##################
    # Public methods #
    ##################
    def get_numpy_matrix(self):
        return np.matrix([self.x, self.y, self.z])

    def set(self, x, y, z):
        self._x = x
        self._y = y
        self._z = z
