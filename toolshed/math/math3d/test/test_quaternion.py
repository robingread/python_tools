import unittest
from toolshed.math.math3d import Quaternion, normalize_quaternion


class TestQuaternion(unittest.TestCase):
    def test_quaternion_init(self):
        q = Quaternion(x=1.0, y=0.0, z=1.0, w=0.0)
        self.assertEqual(q.x, 1.0)
        self.assertEqual(q.y, 0.0)
        self.assertEqual(q.z, 1.0)
        self.assertEqual(q.w, 0.0)

    def test_set_method(self):
        q = Quaternion(x=0, y=0, z=0, w=0)
        q.set(1, 2, 3, 4)
        ans = Quaternion(x=1.0, y=2.0, z=3.0, w=4.0)

        self.assertAlmostEqual(ans.x, q.x)
        self.assertAlmostEqual(ans.y, q.y)
        self.assertAlmostEqual(ans.z, q.z)
        self.assertAlmostEqual(ans.w, q.w)

    def test_normalize_quaternion(self):
        q = Quaternion(x=1.0, y=0.0, z=1.0, w=0.0)
        ans = Quaternion(x=0.70710678, y=0.0, z=0.70710678, w=0.0)
        q.normalize()

        self.assertAlmostEqual(ans.x, q.x)
        self.assertAlmostEqual(ans.y, q.y)
        self.assertAlmostEqual(ans.z, q.z)
        self.assertAlmostEqual(ans.w, q.w)

    def test_multiply_quaternion(self):
        q1 = Quaternion(x=1.0, y=2.0, z=3.0, w=4.0)
        q2 = Quaternion(x=1.0, y=4.0, z=6.0, w=8.0)

        ret = q2 * q1
        ans = Quaternion(x=12.0, y=35.0, z=46.0, w=5.0)

        self.assertAlmostEqual(ret.x, ans.x)
        self.assertAlmostEqual(ret.y, ans.y)
        self.assertAlmostEqual(ret.z, ans.z)
        self.assertAlmostEqual(ret.w, ans.w)


class TestUnitQuarternion(unittest.TestCase):
    pass