import unittest
from toolshed.math.math3d.poses import EulerCoordinates, EulerAngles, EulerPose6Dof


class TestEulerCoordinates(unittest.TestCase):
    def test_init_default(self):
        e = EulerCoordinates()
        self.assertEquals(e.x, 0.0)
        self.assertEquals(e.y, 0.0)
        self.assertEquals(e.z, 0.0)

    def test_init_specified(self):
        e = e = EulerCoordinates(x=1, y=2, z=3)
        self.assertEquals(e.x, 1)
        self.assertEquals(e.y, 2)
        self.assertEquals(e.z, 3)


class TestEulerAngles(unittest.TestCase):
    def test_init_default(self):
        e = EulerAngles()
        self.assertEquals(e.rx, 0.0)
        self.assertEquals(e.ry, 0.0)
        self.assertEquals(e.rz, 0.0)

    def test_init_specified(self):
        e = EulerAngles(rx=1, ry=2, rz=3)
        self.assertEquals(e.rx, 1)
        self.assertEquals(e.ry, 2)
        self.assertEquals(e.rz, 3)


class TestEulerPose6Dof(unittest.TestCase):
    def test_init_default(self):
        e = EulerPose6Dof()
        self.assertEquals(e.x, 0.0)
        self.assertEquals(e.y, 0.0)
        self.assertEquals(e.z, 0.0)
        self.assertEquals(e.rx, 0.0)
        self.assertEquals(e.ry, 0.0)
        self.assertEquals(e.rz, 0.0)

    def test_init_specified(self):
        e = EulerPose6Dof(x=1, y=2, z=3, rx=4, ry=5, rz=6)
        self.assertEquals(e.x, 1)
        self.assertEquals(e.y, 2)
        self.assertEquals(e.z, 3)
        self.assertEquals(e.rx, 4)
        self.assertEquals(e.ry, 5)
        self.assertEquals(e.rz, 6)
