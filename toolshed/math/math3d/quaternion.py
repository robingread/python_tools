import numpy as np
from math import sqrt
from ..matrix import Matrix


class Quaternion(Matrix):
    def __init__(self, x, y, z, w, *args, **kwargs):
        self._x = x
        self._y = y
        self._z = z
        self._w = w
        super(Quaternion, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "Quaternion(x={p.x:.4f}, y={p.y:.4f}, z={p.z:.4f}, w={p.w:.4f})".format(p=self)

    def __mul__(self, other):
        x0, y0, z0, w0 = other.components
        x1, y1, z1, w1 = self.components

        w = -x1*x0 - y1*y0 - z1*z0 + w1*w0
        x = x1*w0 + y1*z0 - z1*y0 + w1*x0
        y = -x1*z0 + y1*w0 + z1*x0 + w1*y0
        z = x1*y0 - y1*x0 + z1*w0 + w1*z0

        return Quaternion(x=x, y=y, z=z, w=w)

    ##############
    # Properties #
    ##############
    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def z(self):
        return self._z

    @property
    def w(self):
        return self._w

    @property
    def components(self):
        return self.x, self.y, self.z, self.w

    ##################
    # Public methods #
    ##################
    def get_numpy_matrix(self):
        return np.matrix([self.x, self.y, self.z, self.w])

    def set(self, x, y, z, w):
        self._x = x
        self._y = y
        self._z = z
        self._w = w

    def normalize(self):
        d = sqrt(self.x**2 + self.y**2 + self.z**2 + self.w**2)
        self._x /= d
        self._y /= d
        self._z /= d
        self._w /= d


class UnitQuarternion(Quaternion):
    def __init__(self, x=0.0, y=0.0, z=0.0, w=0.0, normalized=False):
        super(UnitQuarternion, self).__init__(x, y, z, w)
        if not normalized and not (x == 0 and y == 0 and z == 0 and w == 1):
            self.normalize()


def normalize_quaternion(quat):
    """
    Normalize a quaternion to make it a unit quaternion.

    Args:
        quat: Quaternion to be normalized

    Returns:
        Quaternion instance.
    """
    mag = np.sqrt(quat.x**2 + quat.y**2 + quat.z**2 + quat.w**2)
    return Quaternion(x=quat.x/mag, y=quat.y/mag, z=quat.z/mag, w=quat.w/mag)
