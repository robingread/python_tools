import numpy as np
from vector3d import Vector3D
from rotation3d import Rotation3D
from ..matrix import Matrix


class Transform3D(Matrix):
    """
    """
    def __init__(self, x=0.0, y=0.0, z=0.0, rx=0.0, ry=0.0, rz=0.0, *args, **kwargs):
        self._translation = Vector3D(x=x, y=y, z=z)
        self._rotation = Rotation3D(rx=rx, ry=ry, rz=rz)

        super(Transform3D, self).__init__(*args, **kwargs)
        self._inverse_matrix = np.linalg.inv(self.matrix)

    ##############
    # Properties #
    ##############
    @property
    def rotation(self):
        """
        """
        return self._rotation

    @property
    def translation(self):
        """
        """
        return self._translation

    @property
    def inverse_matrix(self):
        """
        """
        return self._inverse_matrix

    ##################
    # Public methods #
    ##################
    def get_numpy_matrix(self):
        """
        """
        r = self.rotation.matrix
        t = self.translation.matrix.transpose()
        m = np.hstack((r, t))
        m = np.vstack((m, np.matrix([0, 0, 0, 1])))

        return np.matrix(m)
