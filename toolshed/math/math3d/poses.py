class EulerCoordinates(object):
    """
    """
    def __init__(self, x=0.0, y=0.0, z=0.0, *args, **kwargs):
        super(EulerCoordinates, self).__init__(*args, **kwargs)
        self.x = x
        self.y = y
        self.z = z


class EulerAngles(object):
    """
    """
    def __init__(self, rx=0.0, ry=0.0, rz=0.0, *args, **kwargs):
        super(EulerAngles, self).__init__(*args, **kwargs)
        self.rx = rx
        self.ry = ry
        self.rz = rz


class EulerPose6Dof(EulerCoordinates, EulerAngles):
    """
    """
    def __init__(self, *args, **kwargs):
        super(EulerPose6Dof, self).__init__(*args, **kwargs)
