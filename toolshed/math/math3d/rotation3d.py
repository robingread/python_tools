from toolshed.math.matrix import Matrix
import numpy as np


class Rotation3D(Matrix):
    """
    """
    def __init__(self, rx=0.0, ry=0.0, rz=0.0, *args, **kwargs):
        self._rx = rx
        self._ry = ry
        self._rz = rz
        super(Rotation3D, self).__init__(*args, **kwargs)

    def __repr__(self):
        return "Rotation3D(theta={p.theta} rad, axis={p.rotation_axis})".format(p=self)

    ##############
    # Properties #
    ##############
    @property
    def rx(self):
        return self._rx

    @property
    def ry(self):
        return self._ry

    @property
    def rz(self):
        return self._rz

    ##################
    # Public methods #
    ##################
    def get_numpy_matrix(self):
        rot_x = get_x_axis_rotation_matrix(self.rx)
        rot_y = get_y_axis_rotation_matrix(self.ry)
        rot_z = get_z_axis_rotation_matrix(self.rz)
        return rot_z.dot(rot_y.dot(rot_x))


def get_x_axis_rotation_matrix(t):
    """
    Return a numpy rotation matrix - rotation about the x axis
    """
    return np.matrix([[1, 0, 0],
                      [0, np.cos(t), -np.sin(t)],
                      [0, np.sin(t), np.cos(t)]])


def get_y_axis_rotation_matrix(t):
    """
    Return a numpy rotation matrix - rotation about the y axis
    """
    return np.matrix([[np.cos(t), 0, np.sin(t)],
                      [0, 1, 0],
                      [-np.sin(t), 0, np.cos(t)]])


def get_z_axis_rotation_matrix(t):
    """
    Return a numpy rotation matrix - rotation about the z axis
    """
    return np.matrix([[np.cos(t), -np.sin(t), 0],
                      [np.sin(t), np.cos(t), 0],
                      [0, 0, 1]])
