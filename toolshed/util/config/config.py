from .config_errors import ConfigError, ConfigMissingItem


class Config(object):
    """
    """
    def __init__(self, config_file=None, config_data=None, *args, **kwargs):
        if config_file is not None and config_data is not None:
            raise ConfigError()

        if config_file is None and config_data is None:
            raise ConfigError()

        self._config_file = config_file
        self._config_data = config_data

        # Load the config file
        if self._config_file is not None:
            self._on_load_config_file()

    def __repr__(self):
        return "{p.__class__.__name__}({p._config_data})".format(p=self)

    ##############
    # Properties #
    ##############
    @property
    def is_loaded(self):
        return self._config_data is not None

    @property
    def config_file(self):
        return self._config_file

    @property
    def config_data(self):
        return self._config_data

    ##################
    # Public methods #
    ##################
    def get(self, label, required=False, as_config=False):
        return self._get_value(label, required, as_config)

    def get_config_value(self, label, required=False, as_config=False):
        return self._get_value(label, required, as_config)

    ###################
    # Private methods #
    ###################
    def _get_value(self, label, required=False, as_config=False):
        if as_config:
            return self._get_value_as_config(label, required)
        else:
            return self._get_config_value(label, required)

    def _get_config_value(self, label, required):
        try:
            r = self.config_data[label]
            if isinstance(r, unicode):
                return str(r)
            else:
                return r

        except KeyError:
            if required:
                raise ConfigMissingItem()
            else:
                return None

    def _get_value_as_config(self, label, required):
        v = self._get_config_value(label, required)
        if isinstance(v, dict):
            return self.__class__(config_data=v)
        elif required:
            m = 'Unable to return as {0} - "{1}" is not an instance of dict().'.format(self.__class__.__name__, label)
            raise ConfigError(m)
        else:
            return None


    def _on_load_config_file(self):
        """ Override this method """
        pass
