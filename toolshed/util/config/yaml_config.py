from .config import *
import yaml


class YamlConfig(Config):
    def _on_load_config_file(self):
        with open(self._config_file, 'r') as f:
            self._config_data = yaml.load(f)
