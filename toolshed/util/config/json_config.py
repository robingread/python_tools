from .config import Config
import json


class JsonConfig(Config):
    def _on_load_config_file(self):
        with open(self.config_file, 'r') as f:
            self._config_data = json.load(f)
