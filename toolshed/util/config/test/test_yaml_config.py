import unittest

from toolshed.util.config import YamlConfig, ConfigMissingItem


class TestYamlConfig(unittest.TestCase):
    """
    """
    def setUp(self):
        self.config = YamlConfig("test.yaml")

    def test_config_file(self):
        i = self.config.config_file
        exp = "test.yaml"
        self.assertEquals(i, exp)

    def test_config_data(self):
        i = self.config.config_data
        exp = {"name": "Test Yaml File"}
        self.assertEquals(i, exp)

    def test_is_loaded(self):
        i = self.config.is_loaded
        self.assertTrue(True, i)

    def test_get_name(self):
        exp = "Test Yaml File"
        name = self.config.get("name", required=False)
        self.assertEquals(name, exp)

    def test_get_missing_key(self):
        exp = None
        name = self.config.get("this doesn't exist", required=False)
        self.assertEquals(name, exp)

    def test_get_required_missing_key(self):
        self.assertRaises(ConfigMissingItem, self.config.get,
                          label="this doesn't exist", required=True)
