class ConfigError(Exception):
    pass


class ConfigMissingItem(Exception):
    pass
