from scheduler_event import SchedulerEvent
import random
import time
from inspect import isgenerator, isgeneratorfunction


def to_generator(f):
    if not isgenerator(f) and not isgeneratorfunction(f):
        return identity_generator(f)
    else:
        return f


def identity_generator(n):
    while True:
        yield n


def random_generator(min_value, max_value):
    while True:
        # get a normally distributed (more or less) value in [-1, 1]
        n = max(-1.0, min(1.0, random.gauss(mu=0, sigma=1) / 3.0))
        # ...map to [0,1]
        n = (n + 1) / 2
        yield min_value + n * (max_value - min_value)



class Scheduler(object):
    '''Calls registered functions at configurable times in the future.
    
       Example usage: call a function in ten seconds time

           def on_activate(self):
             self.scheduler = Scheduler()
             self.scheduler.schedule(do_something, 10)

           def on_tick(self):
             self.scheduler.run()
       
       do_something will be called if ten seconds have elapsed.     
    '''

    def __init__(self):
        self.events = []
        self.next_event_id = 0

    def run(self):
        """
        Must be called periodically to fire scheduled events, (ie during on_tick)
        """
        [e.fire() for e in self.events if e.should_fire_at(time.time())]
        self.events = [e for e in self.events if e.next_time]

    def schedule(self, fn, interval, repeat=False, replace=None):
        '''Registers a function to call at some point in the future.

           fn       - the function to call
           interval - seconds, a number or a generator which yields numbers
           repeat   - if true the function will be called again using the same 
                      interval, or next interval value if a generator
           replace  - ID of an existing event to cancel           

           Returns a unique ID for the scheduled event.
        '''
        if replace is not None:
            self.cancel(replace)
        e = SchedulerEvent(self.next_event_id, fn, to_generator(interval), repeat)
        self.events.append(e)
        e.schedule()
        self.next_event_id += 1
        return e.event_id

    def repeat(self, fn, interval, replace=None):
        '''Registers a function to call repeatedly until cancelled.

           fn       - the function to call
           interval - seconds, a number or a generator which yields numbers
           replace  - ID of an existing event to cancel           

           Returns a unique ID for the scheduled event.
        '''
        return self.schedule(fn, interval, True, replace)    

    def random(self, fn, min_interval, max_interval, repeat=False, replace=None):
        '''Schedules an event for a random time in the future.

           The time interval has an approximately normal (gaussian) distribution, 
           but is guaranteed to lie in the range [min_interval, max_interval]

           min_interval - minimum seconds between events
           max_interval - maximum seconds between events
           repeat   - if true the function will be called again using the same 
                      interval, or next interval value if a generator
           replace  - ID of an existing event to cancel           

           Returns a unique ID for the scheduled event.
        '''
        return self.schedule(fn, random_generator(min_interval, max_interval), repeat, replace)

    def cancel(self, event_id):
        '''Cancels the given event

           event_id - ID as returned by schedule() 
        '''
        self.events = [e for e in self.events if e.event_id != event_id]

