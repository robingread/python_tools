import time


class SchedulerEvent(object):
    '''A scheduled event'''
    def __init__(self, event_id, fn, interval, repeat=False):
        self.event_id = event_id
        self.fn = fn
        self.interval = interval
        self.repeat = repeat
        self.next_time = None

    def schedule(self):
        self.next_time = time.time() + next(self.interval)

    def should_fire_at(self, t):
        return self.next_time and self.next_time < t

    def fire(self):
        self.fn()

        if self.repeat:
            self.schedule()
        else:
            self.next_time = None
