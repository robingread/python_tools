from datetime import datetime


class Stopwatch(object):
    def __init__(self):
        self._start_time = None
        self._stop_time = None
        self._is_running = False

    def start(self):
        self._start_time = datetime.now()
        self._stop_time = None
        self._is_running = True

    def stop(self):
        self._stop_time = datetime.now()
        self._is_running = False

    def reset(self):
        self.start()

    @property
    def is_running(self):
        return self._is_running

    @property
    def time_elapsed_seconds(self):
        r = None
        if self.is_running:
            r = (datetime.now() - self._start_time).total_seconds()
        else:
            r = (self._stop_time - self._start_time).total_seconds()
        return round(r,3)

    @property
    def time_elapsed_milliseconds(self):
        return self.time_elapsed_seconds * 1000
