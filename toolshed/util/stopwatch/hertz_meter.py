from .stopwatch import Stopwatch


class HertzMeter(Stopwatch):
    def __init__(self):
        super(HertzMeter, self).__init__()

    def get_hz(self):
        if self.is_running:
            try:
                hz = 1.0 / self.time_elapsed_seconds
                return hz
            except ZeroDivisionError:
                return 0
        else:
            return None
