class Event(object):
    def __init__(self):
        self._handlers = set()

    def __call__(self, *args, **kwargs):
        self._fire(*args, **kwargs)

    def __iadd__(self, other):
        return self._handle(other)

    def __isub__(self, other):
        return self._unhandle(other)

    @property
    def num_handlers(self):
        return len(self._handlers)

    def _fire(self, *args, **kwargs):
        for h in self._handlers:
            try:
                h(*args, **kwargs)
            except:
                pass

    def _handle(self, handler):
        self._handlers.add(handler)
        return self

    def _unhandle(self, handler):
        try:
            self._handlers.remove(handler)
        except:
            pass
        return self
