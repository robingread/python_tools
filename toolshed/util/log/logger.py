import time

INFO = "INFO"
DEBUG = "DEBUG"
WARNING = "WARNING"
ERROR = "ERROR"


class Logger(object):
    """
    """
    _all_log_levels = frozenset([INFO, ERROR, WARNING, DEBUG])
    _error_and_warning_log_levels = frozenset([ERROR, WARNING])
    _enabled_log_levels = set([INFO, ERROR, WARNING])

    def __init__(self, log_file_path=None, log_to_stdout=True, log_to_file=True, log_levels=_enabled_log_levels):
        self.log_to_file = log_to_file
        self.log_file_path = log_file_path
        self.log_to_stdout = log_to_stdout
        self._log_file = None

        if bool(self.log_file_path and self.log_to_file):
            try:
                self._log_file = open(log_file_path, 'w+')
            except:
                self._log_to_file = False

        if log_levels is not None:
            self.set_enabled_log_levels(log_levels)

    def info(self, message, context=None):
        self._log(INFO, message, context)

    def info_value(self, label, value, context=None):
        self._log(INFO, "{0}: {1}".format(label, value), context)

    def debug(self, message, context=None):
        self._log(DEBUG, message, context)

    def debug_value(self, label, value, context=None):
        self._log(DEBUG, "{0}: {1}".format(label, value), context)

    def error(self, message, context=None):
        self._log(ERROR, message, context)

    def error_value(self, label, value, context=None):
        self._log(ERROR, "{0}: {1}".format(label, value), context)

    def warning(self, message, context=None):
        self._log(WARNING, message, context)

    def warning_value(self, label, value, context=None):
        self._log(WARNING, "{0}: {1}".format(label, value), context)

    def state(self):
        self.info_value('log to file', self.log_to_file)
        if self.log_to_file:
            self.info_value('log file', self.log_file_path)

    def set_enabled_log_levels(self, levels):
        self._enabled_log_levels = set([l for l in levels if l in self._all_log_levels])

    def get_enabled_log_levels(self):
        return list(self._enabled_log_levels)

    def enable_log_level(self, level):
        if level in self._all_log_levels:
            self._enabled_log_levels.add(level)

    def disable_log_level(self, level):
        self._enabled_log_levels.discard(level)

    def close_log(self):
        if self._log_file:
            self._log_file.close()
            self._log_file = None

    def _log(self, log_level,  message, context=None):

        if log_level not in self._enabled_log_levels:
            return

        timestamp = time.strftime("%Y-%m-%d %H:%M:%S")

        if context:
            m = '[{0}] [{1}] [{3}] {2}'.format(timestamp, log_level, message, context)
        else:
            m = '[{0}] [{1}] {2}'.format(timestamp, log_level, message)

        if self.log_to_stdout:
            print(m)

        if self.log_to_file and self._log_file:
            self._log_file.write(m)
            self._log_file.write('\n')
            self._log_file.flush()
