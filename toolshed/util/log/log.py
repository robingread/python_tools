from .logger import Logger


def info(message, context=None):
    _logger.info(message, context)


def info_value(label, value, context=None):
    _logger.info_value(label, value, context)


def debug(message, context=None):
    _logger.debug(message, context)


def debug_value(label, value, context=None):
    _logger.debug_value(label, value, context)


def error(message, context=None):
    _logger.error(message, context)


def error_value(label, value, context=None):
    _logger.error_value(label, value, context)


def warning(message, context=None):
    _logger.warning(message, context)


def warning_value(label, value, context=None):
    _logger.warning_value(label, value, context)


def state():
    _logger.state()


def set_enabled_log_levels(levels):
    _logger.set_enabled_log_levels(levels)


def get_enabled_log_levels():
    return _logger.get_enabled_log_levels()


def enable_log_level(level):
    _logger.enable_log_level(level)


def disable_log_level(level):
    _logger.disable_log_level(level)


def init(log_file_path=None, log_to_stdout=True, log_to_file=True, log_levels=Logger._enabled_log_levels):
    _logger.close_log()
    _logger.__init__(log_file_path=log_file_path, log_to_stdout=log_to_stdout, log_to_file=log_to_file, log_levels=log_levels)


def close():
    _logger.close_log()


_logger = Logger()
