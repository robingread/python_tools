import numpy as np


class DataError(Exception):
    """
    """
    pass


class Data2D(object):
    """
    """
    def __init__(self, data=None, header=None):
        self._data = data
        self._header = None
        self._header_index_dict = None

        if self.is_loaded and isinstance(header, list):
            self.set_data(data=data, header=header)

    def __setitem__(self, key, value):
        # TODO - be able to set a new column (with int key) via this method
        if not self.is_loaded:
            raise DataError("Can not set data - no data loaded.")

        if isinstance(key, str):
            if key in self._header_index_dict.keys():
                i = self._header_index_dict[key]

                if value is None:
                    raise NotImplementedError("This implementation doesn't work!")
                    i = self._header_index_dict[key]
                    self._header.remove(key)
                    self._header_index_dict[key] = None
                    self._data = np.delete(self._data, i, 1)
                else:
                    self._data[:, i] = value

            else:
                self._add_column_with_header(data=value, header=key)

        elif isinstance(key, int):
            self._data[:, key] = value

        else:
            raise TypeError("Specify either an int or a str.")

    def __getitem__(self, item):
        if not self.is_loaded:
            raise DataError("Can not return data - no data loaded.")

        if isinstance(item, str):
            index = self._header_index_dict[item]
        elif isinstance(item, int):
            index = item
        else:
            raise TypeError("Specify either an int or a str.")

        return np.array(self.data[:, index].flatten('F'), dtype='float')

    ##############
    # Properties #
    ##############
    @property
    def data(self):
        """
        """
        return self._data

    @property
    def header(self):
        """
        """
        return self._header

    @property
    def is_loaded(self):
        """
        """
        return self.data is not None

    @property
    def has_header(self):
        """
        """
        return self.header is not None

    ##################
    # Public methods #
    ##################
    def set_data(self, data, header=None):
        """
        Args:
            data: 2D numpy matrix
            header: List of strings specifying the names of the columns
        """
        self._check_data_is_valid(data)
        self._data = data
        self._header = header

        if not self.has_header:
            return

        # Get column index from the header list
        self._header_index_dict = dict()
        for h in self.header:
            self._header_index_dict[h] = self.header.index(h)

    ###################
    # Private methods #
    ###################
    def _add_column_with_header(self, data, header):
        """
        Args:
            data: Column data to be added (must be a numpy ndarray/matrix)
            header: Value to use as the header.
        """
        self._header.append(header)
        self._header_index_dict[header] = self.header.index(header)
        self._data = np.hstack((self._data, data))

    def _check_data_is_valid(self, data):
        """
        """
        if not isinstance(data, np.ndarray):
            raise DataError("Data needs to be a 2D Numpy array or matrix")

        if data.ndim != 2:
            raise DataError("Data needs to be a 2D Numpy array or matrix")
