from data_2d import Data2D
import csv
import numpy as np


class DataFileError(Exception):
    pass


class DataFile2D(Data2D):
    """
    """
    def __init__(self, file_path=None, delimiter=',', *args, **kwargs):
        super(DataFile2D, self).__init__(*args, **kwargs)
        self._file_path = file_path
        if isinstance(file_path, str):
            self.read_file(file_path=file_path, delimiter=delimiter)

    ##################
    # Public methods #
    ##################
    def read_file(self, file_path, delimiter=',', has_header=True):
        """
        Args:
            file_path:
            delimiter:
            has_header:
        """
        self._file_path = file_path
        with open(file_path, 'r') as f:
            reader = csv.reader(f, delimiter=delimiter)
            header = None
            data = []
            for row in reader:
                if has_header and header is None:
                    header = row
                else:
                    data.append(row)
            data = np.array(data)

            if header is not None:
                self.set_data(data=data, header=header)
            else:
                self.set_data(data=data)

    def write_file(self, file_path, delimiter=','):
        """
        Args:
            file_path:
        """
        with open(file_path, 'w') as f:
            writer = csv.writer(f, delimiter=delimiter)

            if self.has_header:
                writer.writerow(self.header)

            for r in self.data.tolist():
                writer.writerow(r)
