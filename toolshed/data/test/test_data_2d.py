import unittest
from toolshed.data import Data2D
import numpy as np


class TestData2d(unittest.TestCase):
    """
    """
    def _get_test_data(self):
        return np.matrix([[1, 2], [3, 4]])

    def _get_test_header(self):
        return ["a", "b"]

    def _get_data_object(self, data=True, header=False):
        d = self._get_test_data() if data else None
        h = self._get_test_header() if header else None
        return Data2D(data=d, header=h)

    ################
    # Test methods #
    ################
    def test_init_default(self):
        """
        """
        d = Data2D()

        self.assertFalse(d.is_loaded)
        self.assertFalse(d.has_header)
        self.assertIsNone(d.data)
        self.assertIsNone(d.header)

    def test_init_no_header(self):
        """
        """
        a = self._get_test_data()
        d = self._get_data_object(data=True, header=False)

        self.assertTrue(d.is_loaded)
        self.assertFalse(d.has_header)
        self.assertIsNotNone(d.data)
        self.assertIsNone(d.header)

        self.assertEquals(a.all(), d.data.all())

    def test_init_with_header(self):
        """
        """
        a = self._get_test_data()
        h = self._get_test_header()
        d = self._get_data_object(data=True, header=True)

        self.assertTrue(d.is_loaded)
        self.assertTrue(d.has_header)
        self.assertIsNotNone(d.data)
        self.assertIsNotNone(d.header)

        self.assertEquals(a.all(), d.data.all())
        self.assertEquals(h, d.header)

    def test_set_data_no_header(self):
        """
        """
        a = self._get_test_data()
        d = self._get_data_object(data=False, header=False)
        d.set_data(data=a)

        self.assertTrue(d.is_loaded)
        self.assertFalse(d.has_header)
        self.assertIsNotNone(d.data)
        self.assertIsNone(d.header)

        self.assertEquals(a.all(), d.data.all())

    def test_set_data_with_header(self):
        """
        """
        a = self._get_test_data()
        h = self._get_test_header()
        d = self._get_data_object(data=False, header=False)
        d.set_data(data=a, header=h)

        self.assertTrue(d.is_loaded)
        self.assertTrue(d.has_header)
        self.assertIsNotNone(d.data)
        self.assertIsNotNone(d.header)

        self.assertEquals(a.all(), d.data.all())
        self.assertEquals(h, d.header)

    def test_get_item_str(self):
        """
        """
        a = self._get_test_data()
        d = self._get_data_object(data=True, header=True)

        self.assertEquals(a[:, 0].all(), d["a"].all())
        self.assertEquals(a[:, 1].all(), d["b"].all())

    def test_get_item_int(self):
        """
        """
        a = self._get_test_data()
        d = self._get_data_object(data=True, header=True)

        self.assertEquals(a[:, 0].all(), d[0].all())
        self.assertEquals(a[:, 1].all(), d[1].all())

    def test_add_column(self):
        """
        """
        a = np.matrix([[5], [6]])
        h = "c"
        d = self._get_data_object(data=True, header=True)
        d[h] = a

        self.assertEquals(d.header, ["a", "b", "c"])
        self.assertEquals(d.data.all(), np.matrix([[1, 2, 5],
                                                   [3, 4, 6]]).all())

    def test_delete_column(self):
        # a = self._get_test_data()
        # d = self._get_data_object(data=True, header=True)
        # d["a"] = None
        #
        # self.assertEquals(d.header, ["b"])
        # self.assertEquals(d.data.all(), np.matrix([[2],
        #                                            [4]]).all())
        pass
