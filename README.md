# Toolshed - Useful Python Tools

## Installing

Add the directory to your ``PYTHONPATH`` in your ``.basrc`` or ``.bash_profile`` file:

    export PYTHONPATH=/path_to_toolshed/:${PYTHONPATH}
    
## Dependancies

Pandas
NumPy
MatPlotLib